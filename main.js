var imageOfSignature = new Image();
var imageCanvas = new Image();
var __PDF_DOC,
	__CURRENT_PAGE,
	__TOTAL_PAGES,
	__PAGE_RENDERING_IN_PROGRESS = 0,
	__CANVAS = $('#pdf-canvas').get(0),
	__CANVAS_CTX = __CANVAS.getContext('2d');
$('#pop_Up').hide();
$('#canvas_contact').hide();
$('#canvas_value').hide();
$('#canvas_doc').hide();
$('#pdf-canvas-final').hide();
// $('#canvas_value').hide();
function showPDF(pdf_url) {
	$("#pdf-loader").show();

	PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
		__PDF_DOC = pdf_doc;
		__TOTAL_PAGES = __PDF_DOC.numPages;

		// Hide the pdf loader and show pdf container in HTML
		$("#pdf-loader").hide();
		$("#pdf-contents").show();
		$("#pdf-total-pages").text(__TOTAL_PAGES);

		// Show the first page
		showPage(1);
	}).catch(function(error) {
		// If error re-show the upload button
		$("#pdf-loader").hide();
		$("#upload-button").show();

		alert(error.message);
	});;
}


function showPage(page_no) {
	__PAGE_RENDERING_IN_PROGRESS = 1;
	__CURRENT_PAGE = page_no;

	// Disable Prev & Next buttons while page is being loaded
	$("#pdf-next, #pdf-prev").attr('disabled', 'disabled');

	// While page is being rendered hide the canvas and show a loading message
	$("#pdf-canvas").hide();
	$("#page-loader").show();
	$("#download-image").hide();

	// Update current page in HTML
	$("#pdf-current-page").text(page_no);

	// Fetch the page
	__PDF_DOC.getPage(page_no).then(function(page) {
		// As the canvas is of a fixed width we need to set the scale of the viewport accordingly
		var scale_required = __CANVAS.width / page.getViewport(1).width;

		// Get viewport of the page at required scale
		var viewport = page.getViewport(scale_required);

		// Set canvas height
		__CANVAS.height = viewport.height;

		var renderContext = {
			canvasContext: __CANVAS_CTX,
			viewport: viewport
		};

		// Render the page contents in the canvas
		page.render(renderContext).then(function() {
			__PAGE_RENDERING_IN_PROGRESS = 0;

			// Re-enable Prev & Next buttons
			$("#pdf-next, #pdf-prev").removeAttr('disabled');

			// Show the canvas and hide the page loader
			$("#pdf-canvas").show();
			$("#page-loader").hide();
			$("#download-image").hide();
		});
	});
}

// Upon click this should should trigger click on the #file-to-upload file input element
// This is better than showing the not-good-looking file input element
$("#upload-button").on('click', function() {
	$("#file-to-upload").trigger('click');
});

// When user chooses a PDF file
$("#file-to-upload").on('change', function() {
	// Validate whether PDF
    if(['application/pdf'].indexOf($("#file-to-upload").get(0).files[0].type) == -1) {
        alert('Error : Not a PDF');
        return;
    }

	$("#upload-button").hide();
	$("#drag_button").hide();


	// Send the object url of the pdf
	showPDF(URL.createObjectURL($("#file-to-upload").get(0).files[0]));
});


//Download button
$("#download-image").on('click', function() {
	// $(this).attr('href', __CANVAS.toDataURL()).attr('download', 'page.png');
	
		domtoimage.toPng(
			document.getElementById("pdf-canvas"),
			{  quality: 1}
			)
			.then(function(dataUrl) {
				var doc = new jsPDF();
				var img = new Image();
				img.src = dataUrl;
				img.onload = function() {
					doc.addImage(img, 'PNG', 10, 15);
					doc.save('filename.pdf');
				 };
			 })
			 .catch(function (error) {
				  console.error('oops, something went wrong!', error);
			 });  
	
});

$('#signature').on('click',function(){
	$('#pop_Up').show();
	$('#pdf-main-container').hide();
	$("#download-image").show();
	canvasOn();
});
//
function canvasOn(){
    document.addEventListener('mousedown', startPainting);
    document.addEventListener('mouseup', stopPainting);
    document.addEventListener('mousemove', sketch);
    window.addEventListener('resize', resize);
}

const canvas = document.getElementById('canvas');


// Context for the canvas for 2 dimensional operations
const ctx = canvas.getContext('2d');
const dest = canvas.getContext('2d');

// Resizes the canvas to the available size of the window.
function resize(){
  ctx.canvas.width = window.innerWidth;
  ctx.canvas.height = window.innerHeight;
}

// Stores the initial position of the cursor
let coord = {x:0 , y:0};

// This is the flag that we are going to use to
// trigger drawing
let paint = false;

// Updates the coordianates of the cursor when
// an event e is triggered to the coordinates where
// the said event is triggered.
function getPosition(event){
  coord.x = event.clientX - canvas.offsetLeft;
  coord.y = event.clientY - canvas.offsetTop;
}

// The following functions toggle the flag to start
// and stop drawing
function startPainting(event){
  paint = true;
  getPosition(event);
}
function stopPainting(){
  paint = false;
}

function sketch(event){
  if (!paint) return;
  ctx.beginPath();

  ctx.lineWidth = 3;

  // Sets the end of the lines drawn
  // to a round shape.
  ctx.lineCap = 'round';

  ctx.strokeStyle = 'black';

  // The cursor to start drawing
  // moves to this coordinate
  ctx.moveTo(coord.x, coord.y);

  // The position of the cursor
  // gets updated as we move the
  // mouse around.
  getPosition(event);

  // A line is traced from start
  // coordinate to this coordinate
  ctx.lineTo(coord.x , coord.y);

  // Draws the line.
  ctx.stroke();
}

function putImage(){

	imageOfSignature = canvas.toDataURL("image/png");
	console.log(imageOfSignature);
	imageCanvas =  __CANVAS.toDataURL("image/png").replace("image/png","image/doc");

	document.getElementById("canvas_value").src = imageOfSignature;
	document.getElementById("canvas_doc").src = imageCanvas;

	var sign = document.getElementById("canvas_value");
	document.getElementById("canvas_value").width = 500;
	document.getElementById("canvas_value").height = 500;
	var doc = document.getElementById("canvas_doc");

	var contact = document.getElementById("canvas_contact");

	$('#pop_Up').hide();
	$('#pdf-main-container').show();
	$('#signature').hide();

	var can = document.getElementById("pdf-canvas-final");
	var can_ctx = can.getContext('2d');
	can_ctx.drawImage(doc,10,10,500,500);	
	can_ctx.drawImage(sign,10,10,500,500);				
	// __CANVAS_CTX.drawImage(sign,10,10,100,100);

}
